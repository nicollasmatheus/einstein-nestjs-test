# What is this

This is a NestJS basic CRUD API made for test purpose

## Installation

```bash
npm run install
```

## Usage

```python
npm run start
```

This project will run at the port:3000

## License

[MIT](https://choosealicense.com/licenses/mit/)
