import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello Einstein. NestJS CRUD up & running :)';
  }
}
